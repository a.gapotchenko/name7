<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */
include 'settings.php';
function generate($number)
{
    $arr = array('a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0');
// Генерируем пароль
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
// Вычисляем случайный индекс массива
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    print '
            <form action="admin.php" accept-charset="UTF-8" method="GET">
                    <input type="submit" id="send" class="buttonform" value="Войти как администратор">
        </form>
';


    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[] = 'Спасибо, результаты сохранены.';
        if(strip_tags($_COOKIE['admin']=='1')){
            header('Location:login.php');
        }


        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('<br/> <a href="login.php">войти</a> в аккаунт Логин : <strong>%s</strong>
        <br/> Пароль : <strong>%s</strong> ',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);
    $errors['abil'] = !empty($_COOKIE['abil_error']);
    $errors['year'] = !empty($_COOKIE['year_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Заполните имя корректно </div>';
    }
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Заполните почту </div>';
    }
    if ($errors['sex']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('sex_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите пол </div>';
    }
    if ($errors['bio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('bio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Введите биографию </div>';
    }
    if ($errors['check']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('check_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Подтвердите согласие </div>';
    }
    if ($errors['abil']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('abil_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите сверхспособность </div>';
    }
    if ($errors['year']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('year_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Корректно введите дату рождения   </div>';
    }
    if ($errors['limb']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('limb_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите количество конечностей   </div>';
    }


    // TODO: тут выдать сообщения об ошибках в других полях.
    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['sex_value'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
    $values['bio_value'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
    $values['check_value'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);
    $values['abil_value'] = empty($_COOKIE['abil_val']) ? array() : unserialize(strip_tags($_COOKIE['abil_val']));
    $values['year_value'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
    $values['limb_value'] = empty($_COOKIE['limb_value']) ? '' : strip_tags($_COOKIE['limb_value']);
    // TODO: аналогично все поля.
    $flag = 0;
    foreach($errors as $err){ //Проверка ошибок
        if($err==1)$flag=1;
    }

    if (!$flag&&!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {

        $login = $_SESSION['login'];
        $stmt = $db->prepare("SELECT id FROM users WHERE login = ?");
        $stmt->execute([$login]);
        $user_id='';
        while($row = $stmt->fetch())
        {
            $user_id=$row['id'];
        }

        $request = "SELECT name,email,year,sex,limb,bio,checkbox FROM forms WHERE id = ?";
        $result = $db -> prepare($request);
        $result ->execute([$user_id]);
        $data = $result->fetch(PDO::FETCH_ASSOC);
        $request_abil = "SELECT * from all_abilities where id = ?";
        $result_abil=$db->prepare($request_abil);
        $result_abil->execute([$user_id]);
        $data_abil=$result_abil->fetch();

        $values['abil_value'][0]= $data_abil['ability_god']==0 ? '' : 'god';
        $values['abil_value'][1]=$data_abil['ability_through_walls']==0 ? '' : 't_w';
        $values['abil_value'][2]=$data_abil['ability_levitation']==0 ? '' : 'lev';
        $values['abil_value'][3]=$data_abil['ability_brain']==0 ? '' : 'mind';
        $values['abil_value'][4]=$data_abil['ability_power']==0 ? '' : 'power';

        $values['fio'] = strip_tags($data['name']);
        $values['email'] = strip_tags($data['email']);
        $values['year_value'] = strip_tags($data['year']);
        $values['sex_value'] = strip_tags($data['sex']);
        $values['limb_value'] = strip_tags($data['limb']);
        $values['bio_value'] = strip_tags($data['bio']);
        $values['check_value'] = strip_tags($data['checkbox']);



        $login_ses=strip_tags($_SESSION['login']);
        $uid_ses=strip_tags($_SESSION['uid']);

        echo '
<label style="text-align: center"> Вход выполнен : <br/><strong>Логин</strong>:'.$login_ses.' <br/><strong>Ваш id этой сессии</strong>: '.$uid_ses.' </label>
';
        print '
            <form action="login.php" accept-charset="UTF-8" method="GET">
                    <input type="submit" id="send" class="buttonform" value="Выйти">
            </form>
';
    }else{
        print '

            <form action="login.php" accept-charset="UTF-8" method="GET">
                    <input type="submit" id="send" class="buttonform" value="Авторизация">
            </form>
        ';
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('forms.php');
} else {
    if (!isset($_COOKIE['admin'])) {
        setcookie('admin', '0');
    }

    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio']) || !(preg_match("/^[А-ЯA-Z][а-яa-zА-ЯA-Z\-]{0,}\s[А-ЯA-Z][а-яa-zА-ЯA-Z\-]{1,}(\s[А-ЯA-Z][а-яa-zА-ЯA-Z\-]{1,})?$/u", $_POST['fio'])))
    {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
    }
    (empty($_POST['email'])|| !(preg_match("/^((([0-9A-Za-z]{1}[-0-9A-z.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}.){1,2}[-A-Za-z]{2,})$/u",$_POST['email'])))
    {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio2'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('sex_value', $_POST['radio2'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['textarea1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('bio_value', $_POST['textarea1'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['checkbox'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('check_value', $_POST['checkbox'], time() + 365 * 24 * 60 * 60);
    }
    $flag = 0;
    $abil_mas = $_POST['select1'];
    for ($i = 0; $i < 5; $i++) {
        if ($abil_mas[$i] != NULL) {
            $flag = 1;
        }
    }
    if (!$flag) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('abil_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('abil_val', serialize($_POST['select1']), time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('limb_value', $_POST['radio1'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['birthyear']) || $_POST['birthyear'] < 1920 || $_POST['birthyear'] > 2021) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('year_value', $_POST['birthyear'], time() + 365 * 24 * 60 * 60);
    }


    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    } else {

        //Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error', '', 100000);
        setcookie('abil_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('limb_error', '', 100000);
        //удалить остальные Cookies.
        //
        if (!empty($_COOKIE[session_name()]) &&
            session_start() && !empty($_SESSION['login'])&&!empty($_SESSION['uid'])) {

            $fio = $_POST['fio'];
            $email = $_POST['email'];
            $birthyear = $_POST['birthyear'];
            $sex = $_POST['radio2'];
            $limb = $_POST['radio1'];
            $bio = $_POST['textarea1'];


            $login = $_SESSION['login'];
            $stmt = $db->prepare("SELECT id FROM users WHERE login = ?");
            $stmt->execute([$login]);
            $user_id='';
            while($row = $stmt->fetch())
            {
                $user_id=$row['id'];
            }

            $sql = "UPDATE form SET name=?,email=?,year=?,sex=?,limb=?,bio=? WHERE id=?";
            $stmt = $db->prepare($sql);
            $stmt->execute([$fio,$email,$birthyear,$sex,$limb,$bio,$user_id]);




            $ability_data = ['god','t_w','lev','mind','power'];
            $abilities = $_POST['select1'];
            $ability_insert = [];
            foreach ($ability_data as $ability) {
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;

            }
            $god=$ability_insert['god'];
            $wall=$ability_insert['t_w'];
            $lev=$ability_insert['lev'];
            $mind=$ability_insert['mind'];
            $power=$ability_insert['power'];
            $sql = "UPDATE all_abilities SET ability_god='$god',ability_through_walls='$wall',ability_levitation='$lev',ability_brain='$mind',ability_power='$power' WHERE id='$user_id'";
            $stmt = $db->prepare($sql);
            $stmt->execute();

        }
        else {
            // Генерируем уникальный логин и пароль.
            // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
            $login=generate(rand(1,25));
            

            $pass =generate(rand(1,25));



            $hash_pass=password_hash($pass, PASSWORD_DEFAULT);
            // Сохраняем в Cookies.
            setcookie('login', $login);
            setcookie('pass', $pass);

            // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
            // complete


            $fio = $_POST['fio'];
            $email = $_POST['email'];
            $birthyear = $_POST['birthyear'];
            $sex = $_POST['radio2'];
            $limb = $_POST['radio1'];
            $bio = $_POST['textarea1'];
            $check=$_POST['checkbox'];
            $stmt = $db->prepare("INSERT INTO forms (name, year,email,sex,limb,bio,checkbox) VALUES (:fio, :birthyear,:email,:sex,:limb,:bio,:checkbox)");
            $stmt->execute(array('fio' => $fio, 'birthyear' => $birthyear, 'email' => $email, 'sex' => $sex, 'limb' => $limb, 'bio' => $bio, 'checkbox' => $check));


            $ability_data = ['god','t_w','lev','mind','power'];
            $abilities = $_POST['select1'];
            $ability_insert = [];
            foreach ($ability_data as $ability) {
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;

            }

            $stmt = $db->prepare("INSERT INTO all_abilities (ability_god, ability_through_walls,ability_levitation,ability_brain,ability_power) VALUES (:god, :wall,:lev,:mind,:power)");

            $stmt->execute(array('god'=>$ability_insert['god'],'wall'=>$ability_insert['t_w'],'lev'=>$ability_insert['lev'],'mind'=>$ability_insert['mind'],'power'=>$ability_insert['power']));

            $stmt = $db->prepare("INSERT INTO users (login, hash) VALUES (:login,:hash)");
            $stmt->execute(array('login'=>$login,'hash'=>$hash_pass));


        }
        setcookie('save', '1');
        header('Location: index.php');
    }
}

