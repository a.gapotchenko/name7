<?php
    function output_ab($abil)
    {
        print '<td>';
        $i=0;
        $rus_name=['Бессмертие','Сквозь стены','Левитация','Супермозг','Суперсила'];
        do{
            if($abil[$i]==1)
            {
                print $rus_name[$i];
                print '</br>';
            }
            $i++;
        }while($i<5);
        print '</td>';
    }
    function output_hrefs($id)
    {
        print '<td>';
        print "<a href='delete.php?id=$id'>Удалить</a>";
        print '<br/>';
        print "<a href='change.php?id=$id'>Изменить</a>";
        print '</td>';
    }
    function output_users($data)
    {
        print '<tr><td>';
        print $data['id'];
        print '</td><td>';
        print strip_tags($data['name']);
        print '</td><td>';
        print strip_tags($data['email']);
        print '</td><td>';
        print strip_tags($data['year']);
        print '</td><td>';
        print strip_tags($data['sex']);
        print '</td><td>';
        print strip_tags($data['limb']);
        print '</td><td>';
        print strip_tags($data['bio']);
        print '</td><td>';
        print strip_tags($data['login']);
        print '</td><td>';
        print strip_tags($data['hash']);
        print '</td>';
    }
    function output_count($god,$wall,$lev,$br,$pw)
    {
        print '<h2>Статистика по сверхспособностям:</h2>';
        print '<table class="table">';
        print '
                                        <tr>
                                        <th>Бессмертие</th>
                                        <th>Прохождение сквозь стены</th>
                                        <th>Левитация</th>
                                        <th>Супер разум</th>
                                        <th>Супер сила</th>
                                        </tr>';
        print '<tr><td>';
        print $god;
        print '</td><td>';
        print $wall;
        print '</td><td>';
        print $lev;
        print '</td><td>';
        print $br;
        print '</td><td>';
        print $pw;
        print '</td></tr>';
        print '</table>';
    }
