<?php
include 'settings.php';
header('Content-Type: text/html; charset=UTF-8');


$id = $_GET['id'];
$stmt = $db->prepare("SELECT login FROM users WHERE id = ?");
$stmt->execute([$id]);
$user_login='';
while($row = $stmt->fetch())
{
    $user_login=$row['login'];
}
$request = "SELECT * FROM forms where id=?";
$sth = $db->prepare($request);
$sth->execute([$id]);
$data = $sth->fetch(PDO::FETCH_ASSOC);
if($data==false){
    header('Location:admin.php');
    exit();
}
session_start();
$_SESSION['login'] = $user_login;
// Записываем ID пользователя.
$_SESSION['uid'] = $id;

setcookie('admin','1');
// Делаем перенаправление.
header('Location: index.php');
