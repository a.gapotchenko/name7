    <?php
    include 'settings.php';
    include 'output.php';
    if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])) {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="For lab7"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
    $login=$_SERVER['PHP_AUTH_USER'];
    $request = "SELECT * from admin where login =?";
    $result = $db->prepare($request);
    $result->execute([$login]);
    $flag = 0;
    while ($data = $result->fetch()) {
        if ($data['login'] == $_SERVER['PHP_AUTH_USER'] && password_verify($_SERVER['PHP_AUTH_PW'],$data['hash'])) {
            $flag = 1;
        }
    }
    if ($flag == 0) {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm=""');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
    print '<div>Привет админ!</div>';
    session_start();
    if (empty($_SESSION['token'])) {
        $_SESSION['token'] = rand(1,20);
    }
    $token = $_SESSION['token'];

    $request = "SELECT * from form join users on form.id=users.id";
    $result = $db->prepare($request);
    $result->execute();
    $request_abil="SELECT * from all_abilities";
    $request_abil=$db->prepare($request_abil);
    $request_abil->execute();
    ?>
    <head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link  href="style.css" rel="stylesheet"  media="all"/>
    </head>
    <body>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>E-Mail</th>
            <th>Дата рождения</th>
            <th>Пол</th>
            <th>Кол-во конечностей</th>
            <th>Биография</th>
            <th>Логин</th>
            <th>Хэш</th>
            <th>Способности</th>
            <th>Кнопки</th>
        </tr>
        <?php
                while (($data = $result->fetch())) {
                    output_users($data);
                    $abil=$request_abil->fetch();
                    output_ab($abil);
                    output_hrefs($data['id']);

                }
                print '</table>';

                $request = "SELECT COUNT(ability_god) FROM all_abilities WHERE ability_god=1 GROUP BY ability_god ";
                $result = $db->prepare($request);
                $result->execute();
                $god = $result->fetch()[0];
                if($god==NULL)
                {
                    $god=0;
                }
                $request = "SELECT COUNT(ability_through_walls) FROM all_abilities WHERE ability_through_walls=1 GROUP BY ability_through_walls ";
                $result = $db->prepare($request);
                $result->execute();
                $wall = $result->fetch()[0];
                if($wall==NULL)
                {
                    $wall=0;
                }
                $request = "SELECT COUNT(ability_levitation) FROM all_abilities WHERE ability_levity=1 GROUP BY ability_levity ";
                $result = $db->prepare($request);
                $result->execute();
                $lev = $result->fetch()[0];
                if($lev==NULL)
                {
                    $lev=0;
                }
                $request = "SELECT COUNT(ability_brain) FROM all_abilities WHERE ability_brain=1 GROUP BY ability_brain";
                $result = $db->prepare($request);
                $result->execute();
                $br = $result->fetch()[0];
                if($br==NULL)
                {
                    $br=0;
                }
                $request = "SELECT COUNT(ability_power) FROM all_abilities WHERE ability_power=1 GROUP BY ability_power";
                $result = $db->prepare($request);
                $result->execute();
                $pw = $result->fetch()[0];
                if($pw==NULL)
                {
                    $pw=0;
                }
                output_count($god,$wall,$lev,$br,$pw);

        ?>
    </body>

