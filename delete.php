<?php
include 'settings.php';
header('Content-Type: text/html; charset=UTF-8');



$id = $_GET['id'];

$req = "DELETE FROM all_abilities WHERE id=?";
$res = $db->prepare($req);
$res->execute([$id]);
$req = "DELETE FROM forms WHERE id=?";
$res = $db->prepare($req);
$res->execute([$id]);
$req = "DELETE FROM users WHERE id=?";
$res = $db->prepare($req);
$res->execute([$id]);


header('Location:admin.php');

