
<head>
    <meta charset="utf-8"/>

    <title>Дз 7</title>
    <link rel="stylesheet" media="all" href="style.css"/>
    <style>
        /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
        .error {
            border: 2px solid red;
        }
        .error-radio
        {
            border: 2px solid red;
            width: 150px;
        }
        .error-bio
        {
            border: 2px solid red;
            width: 250px;
            height: 16px;
        }
        .error-check
        {
            border: 2px solid red;
            width: 350px;
        }
        .error-abil
        {
            border: 2px solid red;
            width: 290px;
            height: 70px;
        }
        .error-radio-limb
        {
            border: 2px solid red;
            width: 250px;
            height:35px;
        }
    </style>
</head>
<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

<form action="index.php" id="my-formcarry" accept-charset="UTF-8" class="main" method="POST">
    <input style="margin-bottom : 1em" id="formname" type="text" name="fio" placeholder="ФИО"
        <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>">
    <input style="margin-bottom : 1em;margin-top : 1em" id="formmail" type="email" name="email"
           placeholder="Почта"
        <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
    >
    <label >
        Выберите свой год рождения:<br/>
        <input <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year_value']; ?>" id="dr" name="birthyear"
        value=""
        type="number"/>
    </label><br/>

    Выберите пол:<br/>
    <label <?php if($errors['sex']){print 'class="error-radio"';}?>>
        <input <?php if($values['sex_value']=="man"){print 'checked';}?> type="radio"
                                                                         name="radio2" value="man"/>
        Мужской</label>
    <label <?php if($errors['sex']){print 'class="error-radio"';}?>> <input <?php if($values['sex_value']=="woman"){print 'checked';}?> type="radio"
                                                                                                                                        name="radio2" value="woman"/>
        Женский</label>
    <br/>
    <div <?php if($errors['limb']){print 'class="error-radio-limb"';}?>>
        Сколько у вас конечностей :<br/>
        <label><input
                <?php if($values['limb_value']=="4"){print 'checked';}?>
                    type="radio"
                    name="radio1" value="4"/>
            4</label>
        <label><input
                <?php if($values['limb_value']=="5"){print 'checked';}?>
                    type="radio"
                    name="radio1" value="5"/>
            5</label><br>
    </div>
    <label>
        Ваши сверхспособности:
        <br/>
        <div <?php if ($errors['abil']) {print 'class="error-abil"';} ?>> <select id="sp" name="select1[]"
                                                                                  multiple="multiple">
                <option <?php if(in_array('god',$values['abil_value'])){print 'selected';}?> value="god">Бессмертие</option>
                <option <?php if(in_array('t_w',$values['abil_value'])){print 'selected';}?> value="t_w">Прохожение сквозь стены</option>
                <option <?php if(in_array('lev',$values['abil_value'])){print 'selected';}?> value="lev">Левитация</option>
                <option <?php if(in_array('mind',$values['abil_value'])){print 'selected';}?> value="mind">Супер разум</option>
                <option <?php if(in_array('power',$values['abil_value'])){print 'selected';}?> value="power">Супер сила</option>

            </select> </div>
    </label><br/>

    <label <?php if ($errors['bio']) {print 'class="error-bio"';} ?>>
        Ваша биография: <br/>
        <textarea id="biog" name="textarea1" placeholder="Здесь"><?php print $values['bio_value'];?></textarea>
    </label><br/>

    <div <?php if ($errors['check']) {print 'class="error-check"';} ?>><label><input <?php if($values['check_value']=="1"){print 'checked';}?> style="margin-bottom : 1em;margin-top : 1em;" id="formcheck" type="checkbox" name="checkbox"
                                                                                                                                               value="1">Согласие на обработку персональных данных</label></div>

    <input type="submit" id="formsend" class="buttonform" value="Отправить">
</form>
</body>



